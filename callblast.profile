<?php
// $Id$

//@todo: VoIP Drupal setup during install?
//@todo: permissions and roles

/**
 * Name of profile; visible in profile selection form.
 */
define('PROFILE_NAME', 'Callblast');

/**
 * Description of profile; visible in profile selection form.
 */
define('PROFILE_DESCRIPTION', 'Callblast is open-source ready to use system built on Drupal, for delivering voice and SMS to a group of people.');

/**
 * Implementation of hook_form_alter().
 */
function callblast_form_install_configure_form_alter(&$form, $form_state) {
  // Add an additional submit handler.
  $form['#submit'][] = 'callblast_form_submit';
}

/**
 * Custom form submit handler for configuration form.
 *
 */
function callblast_form_submit($form, &$form_state) {
  //Variables
  callblast_variable_defaults();

  // Create a Callblast links in the main menu.
  $items = array(
    array(
      'link_title' => st('Home'),
      'link_path' => '<front>',
      'menu_name' => 'main-menu',
    ),
    array(
      'link_title' => st('Step 1: Import contacts'),
      'link_path' => 'import/node',
      'menu_name' => 'main-menu',
    ),
    array(
      'link_title' => st('Step 2: Manage contacts'),
      'link_path' => 'contacts',
      'menu_name' => 'main-menu',
    ),
    array(
      'link_title' => st('Step 3: Send blast'),
      'link_path' => 'node/add/broadcast',
      'menu_name' => 'main-menu',
    ),
    array(
      'link_title' => st('Step 4: Blast history and management'),
      'link_path' => 'admin/voip/blast/history',
      'menu_name' => 'main-menu',
    ),
  );

  foreach($items as $item) {
    menu_link_save($item);
  }

  // Update the menu router information.
  menu_rebuild();
}

function callblast_variable_defaults() {
  //VoIP Blast module
  variable_set('voipblast_max_length', 15);

  //VoIP Drupal module
  variable_set('voipcall_inbound_call_script_name', 'audio_announcements_main_menu_script');
  variable_set('voipcall_inbound_text_script_name', 'audio_announcements_main_menu_script');

  //SMS Actions module
  variable_set('sms_actions_action_help', '?, h');
  variable_set('sms_actions_action_subscribe', 'subscribe, j');
  variable_set('sms_actions_action_unsubscribe', 's, u');
  variable_set('sms_actions_opt_message', 'Callblast App would like to send you SMS reminders @ this number. X/week. H for help. S to end. Msg&data rates may apply. Reply J to confirm today.');
  variable_set('sms_actions_sms_help', 'Welcome to Callblast! To JOIN text "J". To STOP receiving text messages, text "S". To add new idea, text "I". For HELP, text "H". Msg & data rates may apply.');
}