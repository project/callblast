core = 7.x
api = 2

; Modules

projects[views_bulk_operations][version] = "3.3"

projects[admin_menu][version] = "3.0-rc5"

projects[advancedqueue][version] = "1.0-beta3"

projects[audiofield][version] = "1.2"

projects[audiorecorderfield][version] = "1.0-beta4"

projects[ctools][version] = "1.10"

projects[draggableviews][version] = "2.1"

projects[email][version] = "1.3"

projects[entity][version] = "1.8"

projects[features][version] = "2.10"

projects[feeds][version] = "2.0-beta2"

projects[feeds_tamper][version] = "1.1"

projects[job_scheduler][version] = "2.0-alpha3"

projects[maxlength][version] = "3.2"

projects[strongarm][version] = "2.0"

projects[token][version] = "1.6"

projects[views][version] = "3.14"

projects[views_data_export][version] = "3.1"

projects[voipdrupal][version] = "1.x-dev"

projects[voipblast][version] = "1.0-beta1"

projects[voipnumber][version] = "2.0-beta2"

projects[voipnumberfield][version] = "1.0-beta3"

projects[voipqueue][version] = "1.0-beta5"