
(function ($) {

  Drupal.behaviors.callblast_alters = {
    attach: function (context) {

      $('.page-node-add-contact fieldset.node-form-options', context).drupalSetSummary(function (context) {
        var vals = [];

        $('input:checked', context).parent().each(function () {
          vals.push(Drupal.checkPlain($.trim($(this).text())));
        });

        if (!$('.page-node-add-contact .form-item-status input', context).is(':checked')) {
          vals.unshift(Drupal.t('Send invitation'));
        }
        return vals.join(', ');
      });
    }
  };

})(jQuery);
