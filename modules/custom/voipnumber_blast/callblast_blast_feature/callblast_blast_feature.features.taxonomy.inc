<?php
/**
 * @file
 * callblast_blast_feature.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function callblast_blast_feature_taxonomy_default_vocabularies() {
  return array(
    'groups' => array(
      'name' => 'Groups',
      'machine_name' => 'groups',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
