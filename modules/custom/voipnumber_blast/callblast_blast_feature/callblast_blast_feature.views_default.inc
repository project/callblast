<?php
/**
 * @file
 * callblast_blast_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function callblast_blast_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'callblast_blast_history';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'advancedqueue';
  $view->human_name = 'Blast History';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Blast history and management';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    4 => '4',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'item_id' => 'item_id',
    'views_bulk_operations' => 'views_bulk_operations',
    'created' => 'created',
    'status' => 'status',
    'title' => 'title',
    'processed' => 'processed',
    'name' => 'name',
    'tag' => 'tag',
  );
  $handler->display->display_options['style_options']['default'] = 'created';
  $handler->display->display_options['style_options']['info'] = array(
    'item_id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'processed' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'tag' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Queue: Item ID */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  /* Field: Bulk operations: Queue */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: Queue: Created date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  /* Field: Queue: Item status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  /* Field: Queue: Item title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  /* Field: Queue: Processed date */
  $handler->display->display_options['fields']['processed']['id'] = 'processed';
  $handler->display->display_options['fields']['processed']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['processed']['field'] = 'processed';
  $handler->display->display_options['fields']['processed']['date_format'] = 'short';
  /* Field: Queue: Queue name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Queue: Tag */
  $handler->display->display_options['fields']['tag']['id'] = 'tag';
  $handler->display->display_options['fields']['tag']['table'] = 'advancedqueue_tags';
  $handler->display->display_options['fields']['tag']['field'] = 'tag';
  /* Sort criterion: Queue: Item status */
  $handler->display->display_options['sorts']['status']['id'] = 'status';
  $handler->display->display_options['sorts']['status']['table'] = 'advancedqueue';
  $handler->display->display_options['sorts']['status']['field'] = 'status';
  /* Sort criterion: Queue: Processed date */
  $handler->display->display_options['sorts']['processed']['id'] = 'processed';
  $handler->display->display_options['sorts']['processed']['table'] = 'advancedqueue';
  $handler->display->display_options['sorts']['processed']['field'] = 'processed';
  $handler->display->display_options['sorts']['processed']['group_type'] = 'max';
  $handler->display->display_options['sorts']['processed']['order'] = 'DESC';
  /* Filter criterion: Queue: Queue name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'advancedqueue';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['operator'] = 'starts';
  $handler->display->display_options['filters']['name']['value'] = 'voipblast';
  /* Filter criterion: Queue: Tag */
  $handler->display->display_options['filters']['tag']['id'] = 'tag';
  $handler->display->display_options['filters']['tag']['table'] = 'advancedqueue_tags';
  $handler->display->display_options['filters']['tag']['field'] = 'tag';
  $handler->display->display_options['filters']['tag']['operator'] = '!=';
  $handler->display->display_options['filters']['tag']['value'] = 'optin';
  /* Filter criterion: Queue: Item status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'advancedqueue';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['operator'] = 'not in';
  $handler->display->display_options['filters']['status']['value'] = array(
    2 => '2',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: VoIP queue operations: Queue */
  $handler->display->display_options['fields']['voipblast']['id'] = 'voipblast';
  $handler->display->display_options['fields']['voipblast']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['voipblast']['field'] = 'voipblast';
  $handler->display->display_options['fields']['voipblast']['label'] = '';
  $handler->display->display_options['fields']['voipblast']['element_label_colon'] = FALSE;
  /* Field: Queue: Tag */
  $handler->display->display_options['fields']['tag']['id'] = 'tag';
  $handler->display->display_options['fields']['tag']['table'] = 'advancedqueue_tags';
  $handler->display->display_options['fields']['tag']['field'] = 'tag';
  $handler->display->display_options['fields']['tag']['label'] = 'NID';
  $handler->display->display_options['fields']['tag']['exclude'] = TRUE;
  /* Field: Queue: Item status */
  $handler->display->display_options['fields']['status_1']['id'] = 'status_1';
  $handler->display->display_options['fields']['status_1']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['status_1']['field'] = 'status';
  $handler->display->display_options['fields']['status_1']['group_type'] = 'count';
  $handler->display->display_options['fields']['status_1']['label'] = 'Call list';
  $handler->display->display_options['fields']['status_1']['exclude'] = TRUE;
  /* Field: Queue: Processed date */
  $handler->display->display_options['fields']['processed']['id'] = 'processed';
  $handler->display->display_options['fields']['processed']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['processed']['field'] = 'processed';
  $handler->display->display_options['fields']['processed']['group_type'] = 'max';
  $handler->display->display_options['fields']['processed']['label'] = 'Time';
  $handler->display->display_options['fields']['processed']['date_format'] = 'very_short';
  /* Field: Announcement: Blast title */
  $handler->display->display_options['fields']['blast_title']['id'] = 'blast_title';
  $handler->display->display_options['fields']['blast_title']['table'] = 'voipnumber_blast_announcement';
  $handler->display->display_options['fields']['blast_title']['field'] = 'blast_title';
  $handler->display->display_options['fields']['blast_title']['label'] = 'Title';
  $handler->display->display_options['fields']['blast_title']['alter']['alter_text'] = FALSE;
  $handler->display->display_options['fields']['blast_title']['element_label_colon'] = FALSE;
  /* Field: Queue: Item status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['group_type'] = 'min';
  $handler->display->display_options['fields']['status']['label'] = 'Status';
  /* Field: Announcement: Blast type */
  $handler->display->display_options['fields']['blast_type']['id'] = 'blast_type';
  $handler->display->display_options['fields']['blast_type']['table'] = 'voipnumber_blast_announcement';
  $handler->display->display_options['fields']['blast_type']['field'] = 'blast_type';
  $handler->display->display_options['fields']['blast_type']['label'] = 'Type';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['label'] = 'Phone numbers';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '<a href="details/[tag]">[status_1]</a>';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'More';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'More';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'node/[tag]';
  $handler->display->display_options['fields']['nothing']['alter']['alt'] = 'More';
  $handler->display->display_options['path'] = 'admin/voip/blast/history';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Blast history and management';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['callblast_blast_history'] = $view;

  $view = new view();
  $view->name = 'callblasts_phone_numbers_';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Blast details';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Blast details';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_phone_number' => 'field_phone_number',
    'field_blast_type' => 'field_blast_type',
    'php_1' => 'php_1',
    'php_3' => 'php_3',
    'php' => 'php',
    'php_4' => 'php_4',
    'php_5' => 'php_5',
    'php_2' => 'php_2',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_phone_number' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_blast_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'php_1' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'php_3' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'php' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'php_4' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'php_5' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'php_2' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: VoIP Call */
  $handler->display->display_options['fields']['field_voip_call']['id'] = 'field_voip_call';
  $handler->display->display_options['fields']['field_voip_call']['table'] = 'field_data_field_voip_call';
  $handler->display->display_options['fields']['field_voip_call']['field'] = 'field_voip_call';
  $handler->display->display_options['fields']['field_voip_call']['label'] = '';
  $handler->display->display_options['fields']['field_voip_call']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_voip_call']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_voip_call']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  $handler->display->display_options['fields']['field_voip_call']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_voip_call']['delta_offset'] = '0';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Announcement title';
  /* Field: VoIP Call: VoIP Call Destination Number */
  $handler->display->display_options['fields']['voipcall_dest_number']['id'] = 'voipcall_dest_number';
  $handler->display->display_options['fields']['voipcall_dest_number']['table'] = 'voipnumber_blast_voipcall';
  $handler->display->display_options['fields']['voipcall_dest_number']['field'] = 'voipcall_dest_number';
  $handler->display->display_options['fields']['voipcall_dest_number']['label'] = 'Phone Number';
  /* Field: Content: Blast Type */
  $handler->display->display_options['fields']['field_blast_type']['id'] = 'field_blast_type';
  $handler->display->display_options['fields']['field_blast_type']['table'] = 'field_data_field_blast_type';
  $handler->display->display_options['fields']['field_blast_type']['field'] = 'field_blast_type';
  $handler->display->display_options['fields']['field_blast_type']['label'] = 'Announcement type';
  /* Field: VoIP Call: VoIP Call Date Started */
  $handler->display->display_options['fields']['voipcall_date_started_field']['id'] = 'voipcall_date_started_field';
  $handler->display->display_options['fields']['voipcall_date_started_field']['table'] = 'voipnumber_blast_voipcall';
  $handler->display->display_options['fields']['voipcall_date_started_field']['field'] = 'voipcall_date_started_field';
  $handler->display->display_options['fields']['voipcall_date_started_field']['label'] = 'Date Started';
  /* Field: VoIP Call: VoIP Call Date Completed */
  $handler->display->display_options['fields']['voipcall_date_completed_field']['id'] = 'voipcall_date_completed_field';
  $handler->display->display_options['fields']['voipcall_date_completed_field']['table'] = 'voipnumber_blast_voipcall';
  $handler->display->display_options['fields']['voipcall_date_completed_field']['field'] = 'voipcall_date_completed_field';
  $handler->display->display_options['fields']['voipcall_date_completed_field']['label'] = 'Date Completed';
  /* Field: Announcement: Blast status */
  $handler->display->display_options['fields']['blast_status_field']['id'] = 'blast_status_field';
  $handler->display->display_options['fields']['blast_status_field']['table'] = 'voipnumber_blast_announcement';
  $handler->display->display_options['fields']['blast_status_field']['field'] = 'blast_status_field';
  /* Field: Announcement: Blast additional info */
  $handler->display->display_options['fields']['blast_additional_info_field']['id'] = 'blast_additional_info_field';
  $handler->display->display_options['fields']['blast_additional_info_field']['table'] = 'voipnumber_blast_announcement';
  $handler->display->display_options['fields']['blast_additional_info_field']['field'] = 'blast_additional_info_field';
  $handler->display->display_options['fields']['blast_additional_info_field']['label'] = 'Additional info';
  /* Field: VoIP Call: VoIP Call Duration */
  $handler->display->display_options['fields']['voipcall_duration_field']['id'] = 'voipcall_duration_field';
  $handler->display->display_options['fields']['voipcall_duration_field']['table'] = 'voipnumber_blast_voipcall';
  $handler->display->display_options['fields']['voipcall_duration_field']['field'] = 'voipcall_duration_field';
  $handler->display->display_options['fields']['voipcall_duration_field']['label'] = 'Duration';
  /* Field: VoIP Call: VoIP Call id */
  $handler->display->display_options['fields']['voipcall_cid_field']['id'] = 'voipcall_cid_field';
  $handler->display->display_options['fields']['voipcall_cid_field']['table'] = 'voipnumber_blast_voipcall';
  $handler->display->display_options['fields']['voipcall_cid_field']['field'] = 'voipcall_cid_field';
  $handler->display->display_options['fields']['voipcall_cid_field']['label'] = 'Call id';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'broadcast' => 'broadcast',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/voip/blast/details/%';

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['path'] = 'admin/voipblast/csv';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $export['callblasts_phone_numbers_'] = $view;

  $view = new view();
  $view->name = 'current_calls';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'voipqueue_calls';
  $view->human_name = 'Ongoing Calls';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Ongoing Calls';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'cid' => 'cid',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'cid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no calls running at the moment.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Broken/missing handler */
  $handler->display->display_options['relationships']['item_id']['id'] = 'item_id';
  $handler->display->display_options['relationships']['item_id']['table'] = 'voipqueue_calls';
  $handler->display->display_options['relationships']['item_id']['field'] = 'item_id';
  /* Relationship: Broken/missing handler */
  $handler->display->display_options['relationships']['cid']['id'] = 'cid';
  $handler->display->display_options['relationships']['cid']['table'] = 'voipqueue_calls';
  $handler->display->display_options['relationships']['cid']['field'] = 'cid';
  /* Field: Bulk operations: Queue */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'advancedqueue';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['relationship'] = 'item_id';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::voipqueue_hangup_call' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 1,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: Queue: Tag */
  $handler->display->display_options['fields']['tag']['id'] = 'tag';
  $handler->display->display_options['fields']['tag']['table'] = 'advancedqueue_tags';
  $handler->display->display_options['fields']['tag']['field'] = 'tag';
  $handler->display->display_options['fields']['tag']['relationship'] = 'item_id';
  $handler->display->display_options['fields']['tag']['label'] = 'Blast ID';
  $handler->display->display_options['fields']['tag']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['tag']['alter']['path'] = 'node/[tag]';
  /* Field: VoIP call: Destination number */
  $handler->display->display_options['fields']['dest_number']['id'] = 'dest_number';
  $handler->display->display_options['fields']['dest_number']['table'] = 'voipcall';
  $handler->display->display_options['fields']['dest_number']['field'] = 'dest_number';
  $handler->display->display_options['fields']['dest_number']['relationship'] = 'cid';
  /* Field: VoIP call: Start time */
  $handler->display->display_options['fields']['start_time']['id'] = 'start_time';
  $handler->display->display_options['fields']['start_time']['table'] = 'voipcall';
  $handler->display->display_options['fields']['start_time']['field'] = 'start_time';
  $handler->display->display_options['fields']['start_time']['relationship'] = 'cid';
  $handler->display->display_options['fields']['start_time']['date_format'] = 'long';
  $handler->display->display_options['fields']['start_time']['second_date_format'] = 'long';
  /* Field: VoIP call: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'voipcall';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['relationship'] = 'cid';
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['cid']['id'] = 'cid';
  $handler->display->display_options['fields']['cid']['table'] = 'voipqueue_calls';
  $handler->display->display_options['fields']['cid']['field'] = 'cid';
  $handler->display->display_options['fields']['cid']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['cid']['alter']['path'] = 'voipcall/[cid]';
  /* Sort criterion: Broken/missing handler */
  $handler->display->display_options['sorts']['cid']['id'] = 'cid';
  $handler->display->display_options['sorts']['cid']['table'] = 'voipqueue_calls';
  $handler->display->display_options['sorts']['cid']['field'] = 'cid';
  $handler->display->display_options['sorts']['cid']['order'] = 'DESC';
  /* Filter criterion: VoIP call: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'voipcall';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'cid';
  $handler->display->display_options['filters']['status']['operator'] = 'word';
  $handler->display->display_options['filters']['status']['value'] = 'in_progress, ringing';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator_id'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['label'] = 'Status';
  $handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
  $handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Queue: Queue name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'advancedqueue';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['relationship'] = 'item_id';
  $handler->display->display_options['filters']['name']['value'] = 'voipblast_voice';
  $handler->display->display_options['filters']['name']['group'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/voip/blast/ongoing-calls';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Ongoing Calls';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['weight'] = '0';
  $export['current_calls'] = $view;

  return $export;
}
