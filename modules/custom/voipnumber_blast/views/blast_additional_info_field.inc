<?php

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 */
class blast_additional_info_field extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();

    // Override the alter text option to always alter the text.
    $options['alter']['contains']['alter_text'] = array('default' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Remove the checkbox
    unset($form['alter']['alter_text']);
    unset($form['alter']['text']['#dependency']);
    unset($form['alter']['text']['#process']);
  }

  function render($values) {
    $cid = $values->field_data_field_voip_call_field_voip_call_value;
    if($cid) {
      $voipcall = VoipCall::load($cid);
      $blast = node_load(arg(4));
      if(isset($voipcall)) {
        return voipnumber_blast_additional_info($voipcall, $blast);
      }
    }
    return t('Not processed');
  }
}