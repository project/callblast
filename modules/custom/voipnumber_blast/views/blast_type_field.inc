<?php
/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 */
class blast_type_field extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();

    // Override the alter text option to always alter the text.
    $options['alter']['contains']['alter_text'] = array('default' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Remove the checkbox
    unset($form['alter']['alter_text']);
    unset($form['alter']['text']['#dependency']);
    unset($form['alter']['text']['#process']);
  }

  function render($values) {
    $nid = $values->advancedqueue_tags_tag;
    $node = node_load($nid);
    $blast_type = $node->field_blast_type['und'][0]['value'];

    $announcement_type = '';
    switch ($blast_type) {
      case 'sms':
        //SMS announcement
        //fix: for some reason body changed to eng
        $message = $node->body[$node->language][0]['value'];
        $announcement_type = "SMS announcement: '$message'";
        break;
      case 'sms_optin':
        $message = $node->body[$node->language][0]['value'];
        $announcement_type =  "Opt-in invitation: '$message'";
        break;
      case 'voice_text':
        $voice_say = $node->field_voice_announcement['und'][0]['value'];
        $announcement_type =  "Voice announcement using text: '$voice_say'";
        break;
      case 'voice_audio':
        $voice_audio = $node->field_voice_audio['und'][0]['filename'];
        $announcement_type =  "Voice announcement using audio: '$voice_audio'";
        break;
      case 'voice_script':
        $voip_script = $node->field_voip_script['und'][0]['value'];
        $announcement_type =  "Voice announcement using script: '$voip_script'";
        break;
    }
    return $announcement_type;
  }
}