<?php

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 */
class voipcall_date_completed_field extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();

    // Override the alter text option to always alter the text.
    $options['alter']['contains']['alter_text'] = array('default' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Remove the checkbox
    unset($form['alter']['alter_text']);
    unset($form['alter']['text']['#dependency']);
    unset($form['alter']['text']['#process']);
  }

  function render($values) {
    $cid = $values->field_data_field_voip_call_field_voip_call_value;
    if($cid) {
      $voipcall = VoipCall::load($cid);

      if (isset($voipcall) && $voipcall->getEndTime()) {
        return format_date($voipcall->getEndTime(), 'very_short');
      }
    }
    return t('Not processed yet');
  }
}