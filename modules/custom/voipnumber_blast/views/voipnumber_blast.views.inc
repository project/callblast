<?php

/**
 * Implements hook_views_data().
 */
function voipnumber_blast_views_data() {
  $data['voipnumber_blast_announcement']['table']['group'] = t('Announcement');
  $data['voipnumber_blast_announcement']['table']['join'] = array(
    // #global is a special flag which let's a table appear all the time.
    '#global' => array(),
  );

  $data['voipnumber_blast_announcement']['blast_type'] = array(
    'title' => t('Blast type'),
    'help' => t('Blast type.'),
    'field' => array(
      'handler' => 'blast_type_field',
    ),
  );

  $data['voipnumber_blast_announcement']['blast_additional_info_field'] = array(
    'title' => t('Blast additional info'),
    'help' => t('Blast additional info.'),
    'field' => array(
      'handler' => 'blast_additional_info_field',
    ),
  );

  $data['voipnumber_blast_announcement']['blast_status_field'] = array(
    'title' => t('Blast status'),
    'help' => t('Blast status.'),
    'field' => array(
      'handler' => 'blast_status_field',
    ),
  );

  $data['voipnumber_blast_announcement']['blast_title'] = array(
    'title' => t('Blast title'),
    'help' => t('Blast title.'),
    'field' => array(
      'handler' => 'blast_title_field',
    ),
  );

  $data['voipnumber_blast_voipcall']['table']['group'] = t('VoIP Call');
  $data['voipnumber_blast_voipcall']['table']['join'] = array(
    // #global is a special flag which let's a table appear all the time.
    '#global' => array(),
  );
  $data['voipnumber_blast_voipcall']['voipcall_dest_number'] = array(
    'title' => t('VoIP Call Destination Number'),
    'help' => t('VoIP Call Destination Number'),
    'field' => array(
      'handler' => 'voipcall_dest_number_field',
    ),
  );

  $data['voipnumber_blast_voipcall']['voipcall_cid_field'] = array(
    'title' => t('VoIP Call id'),
    'help' => t('VoIP Call id'),
    'field' => array(
      'handler' => 'voipcall_cid_field',
    ),
  );

  $data['voipnumber_blast_voipcall']['voipcall_date_completed_field'] = array(
    'title' => t('VoIP Call Date Completed'),
    'help' => t('VoIP Call Date Completed'),
    'field' => array(
      'handler' => 'voipcall_date_completed_field',
    ),
  );

  $data['voipnumber_blast_voipcall']['voipcall_date_started_field'] = array(
    'title' => t('VoIP Call Date Started'),
    'help' => t('VoIP Call Date Started'),
    'field' => array(
      'handler' => 'voipcall_date_started_field',
    ),
  );

  $data['voipnumber_blast_voipcall']['voipcall_duration_field'] = array(
    'title' => t('VoIP Call Duration'),
    'help' => t('VoIP Call Duration'),
    'field' => array(
      'handler' => 'voipcall_duration_field',
    ),
  );

  return $data;
}


function voipnumber_blast_views_data_alter($data){
  //@todo:
  $data['field_data_field_voip_call']['dummy_name'] = array(
    'title' => t('Map field to other db column'),
    'group' => t('Custom mapping'),
    'relationship' => array(
      'base' => 'voipcall',
      'base field' => 'cid',
      'field' => 'field_voip_call_value',
      'handler' => 'views_handler_relationship',
      'label' => t('Map field to db column'),
      'title' => t('Field to db map'),
    ),
  );

 /* $data['field_data_field_voip_call']['field_voip_call_value']['relationship'] = array(
      'base' => 'voipcall',
      'base field' => 'cid',
      'field' => 'field_voip_call_value',
      'handler' => 'views_handler_relationship',
      'label' => t('Map field to db column'),
      'title' => t('Field to db map'),
  );*/
  $data['field_data_field_voip_call']['table']['join']['voipcall']=array(
    'left_field'=>'cid',
    'field'=>'cid',
  );
}