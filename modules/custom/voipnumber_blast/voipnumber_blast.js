(function ($) {
    Drupal.behaviors.voipnumber_blast = {
        attach: function (context, settings) {
            //Hide admin options from broadcast and episode content types
            $('#broadcast-node-form .vertical-tabs').hide();
            $('#edit-field-blast-type-und').change(function () {
                    if($(this).val() == 'sms_optin') {
                        $('#edit-body-und-0-value').val(Drupal.settings.voipnumber_blast.opt_in_message);
                    }
                    else {
                        $('#edit-body-und-0-value').val('');
                    }
                }
            );
            $('#contacts-preview').click(function (e) {
                var destinationType = $("#edit-field-destination-type-und").val();
                switch(destinationType) {
                    case "all_numbers":
                        $(this).attr("href", Drupal.settings.basePath + "contacts-preview");
                        break;
                    case "selected_groups":
                        var groupIDs = $("#edit-field-select-destination input:checkbox:checked").map(function () {
                            return $(this).val();
                        }).get();
                        var groupIDsquery = groupIDs.join("+");

                        if (!groupIDsquery) {
                            alert("Please select one or more groups.");
                            e.preventDefault();
                        }
                        else {
                            groupIDsquery = groupIDsquery.replace("all+", "");
                            $(this).attr("href", Drupal.settings.basePath + "contacts-preview/" + groupIDsquery);
                        }
                        break;
                }
            });
        }
    };
})(jQuery);